package com.test;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Technology {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Relationship(type = Relationship.OUTGOING)
    private Set<Person> people;

    public Technology() {
    }

    Technology(String name) {
        this.name = name;
    }

    public void addPerson(final Person person) {
        if (people == null) {
            people = new HashSet<>();
        }
        people.add(person);
    }

    @Override
    public String toString() {
        return "Technology{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Person> getPeople() {
        return people;
    }
}

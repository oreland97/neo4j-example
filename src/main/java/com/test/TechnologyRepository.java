package com.test;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface TechnologyRepository extends Neo4jRepository<Technology, Long> {

    Technology findByName(final String name);

}
package com.test;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import java.util.Arrays;

@SpringBootApplication
@EnableNeo4jRepositories
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner runner(final PersonRepository personRepository,
                             final TechnologyRepository technologyRepository) {
        return args -> {
            personRepository.deleteAll();
            technologyRepository.deleteAll();

            Technology java = new Technology("Java");
            Technology spring = new Technology("Spring");
            Technology html = new Technology("Html");

            Arrays.asList(java, spring).forEach(technologyRepository::save);

            Person mike = new Person("Mike", "Ross");
            Person harvey = new Person("Harvey", "Spector");
            Person tommy = new Person("Tommy", "Shelby");

            java.addPerson(mike);
            java.addPerson(harvey);
            html.addPerson(mike);

            spring.addPerson(tommy);

            technologyRepository.save(java);
            technologyRepository.save(spring);
            technologyRepository.save(html);

            technologyRepository.findByName("Java")
                    .getPeople()
                    .forEach(System.out::println);
        };
    }
}

package com.test;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface PersonRepository extends Neo4jRepository<Person, Long> {

    Person findByFirstNameAndLastName(final String firstName, final String lastName);

}